<?php

namespace Drupal\entity_reference_delete;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\UseCacheBackendTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\field\Entity\FieldConfig;

/**
 * Provides a map of info about fields which should be acted on.
 */
class FieldMap {

  use UseCacheBackendTrait;

  /**
   * Key used in bundle lists to indicate all bundles.
   */
  public const ALL_BUNDLES = ':all_bundles:';

  /**
   * The Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Creates a FieldMap instance.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The Entity field manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The Cache backend service.
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager,
    CacheBackendInterface $cache_backend
  ) {
    $this->entityFieldManager = $entity_field_manager;
    $this->cacheBackend = $cache_backend;
  }

  /**
   * Gets a list of fields which delete parents on child deletion.
   *
   * @param string $child_entity_type_id
   *   The ID of the child entity type. For example, if a tags field on nodes is
   *   configured to delete nodes when a term is deleted, pass in
   *   'taxonomy_term' to get this field's details.
   *
   * @return array
   *   A nested array of data about reference fields which are configured to
   *   delete parents on child deletion. The successive nested array keys are:
   *    - The ID of the entity type the field is on, in other words, the
   *      parent entity type.
   *    - The name of the field.
   *    - The bundles of the field which are configured for deletion.
   *      Additionally, the key static::ALL_BUNDLES may be present here with
   *      value TRUE to indicate all bundles are present.
   *   The value below this is an array with these properties:
   *    - 'handling': The value of the 'delete_parents_on_child_delete'
   *      setting on the field.
   *    - 'use_queue': TRUE if a queue should be used; FALSE if not.
   *
   *   So for example, given a field 'terms' on nodes, which exists on node
   *   types 'alpha', 'beta', and 'other', we might have this structure to show
   *   that deleting a taxonomy term should cause the referencing node to be
   *   deleted:
   *   @code
   *     'node' => [
   *       'terms' => [
   *         'alpha' => // info array
   *         'beta' => // info array
   *       ]
   *     ]
   *   ]
   *   @endcode
   */
  public function getFieldsDeleteParents(string $child_entity_type_id): array {
    $cid = 'entity_reference_delete-parents-fields';

    if ($cache = \Drupal::cache()->get($cid)) {
      $data = $cache->data;
    }
    else {
      // Assemble a map whose successive nested keys are:
      // - target entity type ID of the reference field
      // - entity type ID that the reference field is on
      // - field name.
      $data = [];
      $field_map = $this->entityFieldManager->getFieldMap();
      foreach ($field_map as $entity_type_id => $entity_type_fields) {
        $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_id);
        $base_field_definitions = $this->entityFieldManager->getBaseFieldDefinitions($entity_type_id);

        foreach ($entity_type_fields as $field_name => $field_data) {
          // @todo Expand this to cover all reference field types when
          // https://www.drupal.org/project/drupal/issues/3057545 is fixed.
          if ($field_data['type'] != 'entity_reference') {
            continue;
          }

          // Skip fields that don't have a storage, as we can't query for them.
          if (!isset($field_storage_definitions[$field_name])) {
            continue;
          }

          $field_storage_definition = $field_storage_definitions[$field_name];

          // @todo Use the new interface when
          // https://www.drupal.org/project/drupal/issues/3057545 is fixed.
          $field_target_entity_ids = [$field_storage_definition->getSettings()['target_type']];

          // Handle base and bundle/config fields separately.
          if ($field_storage_definition->isBaseField()) {
            $base_field_definition = $base_field_definitions[$field_name];

            // Computed fields can't be queried.
            if ($base_field_definition->isComputed()) {
              continue;
            }

            $settings = $base_field_definition->getSetting('entity_reference_delete');

            // Skip fields which don't have our settings.
            if (empty($settings['delete_parents_on_child_delete'])) {
              continue;
            }

            foreach ($field_target_entity_ids as $field_target_entity_id) {
              $data[$field_target_entity_id][$entity_type_id][$field_name][static::ALL_BUNDLES] = [
                'handling' => $settings['delete_parents_on_child_delete'],
                'use_queue' => $settings['use_queue_on_parent_delete'] ?? FALSE,
              ];
            }
          }
          else {
            foreach ($field_data['bundles'] as $bundle) {
              $bundle_fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);

              /** @var Drupal\Core\Field\FieldDefinitionInterface $field_definition */
              $field_definition = $bundle_fields[$field_name];

              if ($field_definition instanceof FieldConfig) {
                $settings = $field_definition->getThirdPartySettings('entity_reference_delete');
              }
              elseif ($field_definition instanceof BaseFieldDefinition) {
                // So far, code-based bundle fields in the wild (in the contrib
                // entity module - https://www.drupal.org/project/entity) extend
                // BaseFieldDefinition, so we detect them like this.
                $settings = $field_definition->getSetting('entity_reference_delete');
              }
              else {
                // No idea, alien field class, can't handle this.
                continue;
              }

              // Skip fields which don't have our settings.
              if (empty($settings['delete_parents_on_child_delete'])) {
                continue;
              }

              $bundle_data = [
                'handling' => $settings['delete_parents_on_child_delete'],
                'use_queue' => $settings['use_queue_on_parent_delete'] ?? FALSE,
              ];

              foreach ($field_target_entity_ids as $field_target_entity_id) {
                $data[$field_target_entity_id][$entity_type_id][$field_name][$bundle] = $bundle_data;
              }
            } // foreach bundle
          } // if/else field scope
        } // foreach $field_name
      } // foreach $entity_type_id

      $this->cacheSet($cid, $data, Cache::PERMANENT, [
        'entity_types',
        'entity_field_info',
      ]);
    }

    return $data[$child_entity_type_id] ?? [];
  }

  /**
   * Gets a list of fields which delete children on parent deletion.
   *
   * @param string $parent_entity_type_id
   *   The parent entity type ID.
   *
   * @return array
   *   A nested array of data about reference fields which are configured to
   *   delete children on parent deletion. The successive nested array keys are:
   *    - The name of the field.
   *    - The bundles of the field which are configured for deletion.
   *   Each final array value contains:
   *    - 'handling': The value of the 'delete_parents_on_child_delete'
   *      setting on the field.
   *    - 'use_queue': TRUE if a queue should be used; FALSE if not.
   *    - target_entity_type_id: The ID of the entity type that is being
   *      referenced.
   */
  public function getFieldsDeleteChildren(string $parent_entity_type_id): array {
    $cid = 'entity_reference_delete-children-fields';

    if ($cache = \Drupal::cache()->get($cid)) {
      $data = $cache->data;
    }
    else {
      $data = [];
      $field_map = $this->entityFieldManager->getFieldMap();
      foreach ($field_map as $entity_type_id => $entity_type_fields) {
        $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_id);

        foreach ($entity_type_fields as $field_name => $field_data) {
          // @todo Expand this to cover all reference field types when
          // https://www.drupal.org/project/drupal/issues/3057545 is fixed.
          if ($field_data['type'] != 'entity_reference') {
            continue;
          }

          // Skip fields that don't have a field storage.
          if (!isset($field_storage_definitions[$field_name])) {
            continue;
          }

          foreach ($field_data['bundles'] as $bundle) {
            $bundle_fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);

            /** @var Drupal\Core\Field\FieldDefinitionInterface $field_definition */
            $field_definition = $bundle_fields[$field_name];

            // Skip computed fields.
            if ($field_definition->isComputed()) {
              continue;
            }

            if ($field_definition instanceof FieldConfig) {
              $settings = $field_definition->getThirdPartySettings('entity_reference_delete');
            }
            elseif ($field_definition instanceof BaseFieldDefinition) {
              $settings = $field_definition->getSetting('entity_reference_delete');
            }
            else {
              // No idea, alien field class, can't handle this.
              continue;
            }

            // Skip fields which don't have our settings.
            if (empty($settings['delete_children_on_parent_delete'])) {
              continue;
            }

            $bundle_data = [
              'handling' => $settings['delete_children_on_parent_delete'],
              'use_queue' => $settings['use_queue_on_child_delete'] ?? FALSE,
              // @todo Use the new interface when
              // https://www.drupal.org/project/drupal/issues/3057545 is fixed.
              'target_entity_type_id' => $field_storage_definitions[$field_name]->getSettings()['target_type'],
            ];

            $data[$entity_type_id][$field_name][$bundle] = $bundle_data;
          } // foreach bundle
        } // foreach $field_name
      } // foreach $entity_type_id
    }

    return $data[$parent_entity_type_id] ?? [];
  }

}
