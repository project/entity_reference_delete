<?php

namespace Drupal\Tests\entity_reference_delete\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests deletion of related entities referenced with base fields.
 *
 * The test method is defined in EntityReferenceDeleteTrait as it is common to
 * another test class.
 *
 * @group entity_reference_delete
 */
class EntityReferenceDeleteBaseFieldsTest extends KernelTestBase {

  use EntityReferenceDeleteTrait;

  /**
   * The modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'node',
    'field',
    'text',
    'options',
    'taxonomy',
    'entity_reference_delete',
    'entity_reference_delete_test',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * The queue worker plugin manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * The cron runner.
   *
   * @var \Drupal\Core\CronInterface
   */
  protected $cron;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->queue = $this->container->get('queue');
    $this->queueManager = $this->container->get('plugin.manager.queue_worker');
    $this->cron = $this->container->get('cron');

    $this->installConfig(static::$modules);
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('taxonomy_term');
    $this->installSchema('node', ['node_access']);

    // Create Article content type.
    $node_type = $this->entityTypeManager->getStorage('node_type')->create([
      'type' => 'article',
      'name' => 'Article',
    ]);
    $node_type->save();

    // Create Tags vocabulary.
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->create([
      'name' => 'Tags',
      'vid' => 'tags',
    ]);
    $vocabulary->save();
  }

}
