<?php

namespace Drupal\Tests\entity_reference_delete\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests deletion of related entities referenced with config fields.
 *
 * The test method is defined in EntityReferenceDeleteTrait as it is common to
 * another test class.
 *
 * @group entity_reference_delete
 */
class EntityReferenceDeleteConfigFieldsTest extends KernelTestBase {

  use EntityReferenceDeleteTrait;

  /**
   * The modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'node',
    'field',
    'text',
    'options',
    'taxonomy',
    'entity_reference_delete',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * The queue worker plugin manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * The cron runner.
   *
   * @var \Drupal\Core\CronInterface
   */
  protected $cron;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->queue = $this->container->get('queue');
    $this->queueManager = $this->container->get('plugin.manager.queue_worker');
    $this->cron = $this->container->get('cron');

    $this->installConfig(static::$modules);
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('taxonomy_term');
    $this->installSchema('node', ['node_access']);

    // Create Article content type.
    $node_type = $this->entityTypeManager->getStorage('node_type')->create([
      'type' => 'article',
      'name' => 'Article',
    ]);
    $node_type->save();

    // Create Tags vocabulary.
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->create([
      'name' => 'Tags',
      'vid' => 'tags',
    ]);
    $vocabulary->save();

    // Set up config fields.
    $field_storage_config_storage = $this->entityTypeManager->getStorage('field_storage_config');
    $field_config_storage = $this->entityTypeManager->getStorage('field_config');

    // Create field storages.
    foreach ([
      'ref_no_delete',
      'ref_child_delete_always',
      'ref_parent_delete_always',
    ] as $field_name) {
      /** @var \Drupal\field\Entity\FieldStorageConfig $field_storage */
      $field_storage_config = $field_storage_config_storage->create([
        'field_name' => $field_name,
        'type' => 'entity_reference',
        'entity_type' => 'node',
        'cardinality' => 1,
      ]);
      $field_storage_config
        ->setSetting('target_type', 'taxonomy_term')
        ->setSetting('handler', 'default');
      $field_storage_config->save();
    }

    // Create fields.
    $field_config = $field_config_storage->create([
      'entity_type' => 'node',
      'field_name' => 'ref_no_delete',
      'bundle' => 'article',
      'label' => 'Label',
    ]);
    $field_config->save();

    $field_config = $field_config_storage->create([
      'entity_type' => 'node',
      'field_name' => 'ref_child_delete_always',
      'bundle' => 'article',
      'label' => 'Label',
    ]);
    $field_config->setThirdPartySetting('entity_reference_delete', 'delete_children_on_parent_delete', 'always');
    $field_config->setThirdPartySetting('entity_reference_delete', 'use_queue_on_child_delete', TRUE);
    $field_config->save();

    $field_config = $field_config_storage->create([
      'entity_type' => 'node',
      'field_name' => 'ref_parent_delete_always',
      'bundle' => 'article',
      'label' => 'Label',
    ]);
    $field_config->setThirdPartySetting('entity_reference_delete', 'delete_parents_on_child_delete', 'always');
    $field_config->setThirdPartySetting('entity_reference_delete', 'use_queue_on_child_delete', TRUE);
    $field_config->save();
  }

}
