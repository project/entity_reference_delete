<?php

namespace Drupal\entity_reference_delete\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Deleles child entities in reaction to parent deletion.
 *
 * This runs a limited query for scalability and requeues the current item if
 * more entities remain to be processed.
 *
 * @QueueWorker(
 *   id = "entity_reference_delete_children",
 *   title = "Deletes child entities in reaction to parent deletion.",
 *   cron = {"time" = 60},
 * )
 */
class ChildDeleteQueueWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $child_entity_type_id = $data['child_entity_type_id'];

    $child_entity_type_storage = \Drupal::service('entity_type.manager')->getStorage($child_entity_type_id);

    foreach ($data['field_items'] as $field_item) {
      // @todo The 'target_id' column name only applies for entity_reference
      // fields.
      $child_entity = $child_entity_type_storage->load($field_item['target_id']);
      if (!$child_entity) {
        continue;
      }

      $child_entity->delete();
    }
  }

}
