# Entity Reference Delete

The Entity Reference Delete module allows reference fields to enforce
referential integrity by deleting related entities. References can be enforced
in both directions:

- A parent entity being deleted can cause child entities to be deleted. For
  example, deleting a node could cause the taxonomy terms referenced in a 'tags'
  field to be deleted.
- A child entity being deleted could cause parent entities to be deleted. For
  example, deleting a store entity could cause product entities which reference
  that store to be deleted.

## Other similar modules:

- https://www.drupal.org/project/entity_reference_integrity: Prevents deletion
  of referenced entities.
- https://www.drupal.org/project/entity_reference_purger: Cleans up field values
  in referencing entities that point to deleted entities.
- https://www.drupal.org/project/err_delete: Provides a UI for recursively
  deleting entities.

## Limitations

The module only supports the entity_reference field type in core, and does not
work with dynamic_entity_reference or entity_reference_revisions field types
from contrib. Support for these could be added when core issue
https://www.drupal.org/project/drupal/issues/3057545 is fixed.

# Requirements

This module requires no modules outside of Drupal core.

# Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

# Configuration

To set a config field to delete related entities, edit the field and select
the reference deletion options.

To set a base field or bundle field to delete related entities, either change
its settings in its definition or if the definition is in core or contrib code,
implement hook_entity_base_field_info_alter() in a custom module.
