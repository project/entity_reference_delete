<?php

namespace Drupal\Tests\entity_reference_delete\Kernel;

use Drupal\Core\Entity\EntityInterface;

/**
 * Trait for kernel tests.
 *
 * This has the actual test method, so that the actual test classes only handle
 * the setup. Two different classes are needed because the config test class
 * must not enable the test module that provides base fields.
 */
trait EntityReferenceDeleteTrait {

  /**
   * Tests the deletion of related entities.
   */
  public function testReferenceDeletionBaseFields() {
    // Test deleting a child causes parents to be deleted.
    $node_storage = $this->entityTypeManager->getStorage('node');
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');

    // The child term we'll delete to trigger a node deletion.
    $child_term_to_delete = $term_storage->create([
      'vid' => 'tags',
      'name' => 'Child to delete',
    ]);
    $child_term_to_delete->save();

    // An unrelated term we'll delete to test nothing else happens.
    $term_unrelated = $term_storage->create([
      'vid' => 'tags',
      'name' => 'Unrelated',
    ]);
    $term_unrelated->save();

    // A node that references the term to delete, but via a field that is not
    // configured to cause deletion.
    $node_no_delete = $node_storage->create([
      'type' => 'article',
      'title' => 'Fruits',
      'ref_no_delete' => [$child_term_to_delete],
    ]);
    $node_no_delete->save();

    // A node that references the term to delete, in a field that will cause
    // deletion.
    $node_delete = $node_storage->create([
      'type' => 'article',
      'title' => 'Fruits',
      'ref_parent_delete_always' => [$child_term_to_delete],
    ]);
    $node_delete->save();

    // Nothing gets deleted when deleting an unrelated term. However, a queue
    // item does get created to query for parents.
    $term_unrelated->delete();
    $this->assertEquals(1, $this->queue->get('entity_reference_delete_parent_query')->numberOfItems());
    // Processing the queue has no effect.
    $this->processQueue('entity_reference_delete_parent_query');
    $this->assertCount(2, $node_storage->loadMultiple());
    $this->assertEquals(0, $this->queue->get('entity_reference_delete_parent_query')->numberOfItems());

    // Deleting the child term causes a queue item to be created.
    $child_term_to_delete->delete();
    $this->assertEquals(1, $this->queue->get('entity_reference_delete_parent_query')->numberOfItems());

    // Nothing happens until the queue is processed.
    $this->assertCount(2, $node_storage->loadMultiple());

    // Process the parent reference deletion query queue.
    $this->processQueue('entity_reference_delete_parent_query');

    // The node that referenced the term with a field that doesn't delete is
    // still there.
    $node_no_delete = $this->reloadEntity($node_no_delete);
    $this->assertNotEmpty($node_no_delete);

    // The node that referenced the term with a field that is configured to
    // delete has now been deleted.
    $node_delete = $this->reloadEntity($node_delete);
    $this->assertEmpty($node_delete);

    // Test deleting a parent causes chilren to be deleted.
    // A term that won't be deleted because its field isn't configured to do so.
    $term_no_delete = $term_storage->create([
      'vid' => 'tags',
      'name' => 'No delete',
    ]);
    $term_no_delete->save();

    $term_delete = $term_storage->create([
      'vid' => 'tags',
      'name' => 'Delete',
    ]);
    $term_delete->save();

    $term_unrelated = $term_storage->create([
      'vid' => 'tags',
      'name' => 'Unrelated',
    ]);
    $term_unrelated->save();

    $node = $node_storage->create([
      'type' => 'article',
      'title' => 'Fruits',
      'ref_no_delete' => [$term_no_delete],
      'ref_child_delete_always' => [$term_delete],
    ]);
    $node->save();

    $node_unrelated = $node_storage->create([
      'type' => 'article',
      'title' => 'Fruits',
    ]);
    $node_unrelated->save();

    // Nothing happens when deleting an unrelated node.
    $node_unrelated->delete();
    $this->assertCount(3, $term_storage->loadMultiple());

    // Delete the node. This is the parent of several term reference fields, and
    // should cause some child terms to be deleted or queued for deletion.
    $node->delete();

    // Nothing is deleted until the queue is processed, but the queue should
    // have items.
    $this->assertCount(3, $term_storage->loadMultiple());
    $this->assertEquals(1, $this->queue->get('entity_reference_delete_children')->numberOfItems());

    // Process the parent reference deletion query queue.
    $this->processQueue('entity_reference_delete_children');

    $term_no_delete = $this->reloadEntity($term_no_delete);
    $this->assertNotEmpty($term_no_delete);

    $term_delete = $this->reloadEntity($term_delete);
    $this->assertEmpty($term_delete);
  }

  /**
   * Reloads the given entity from the storage and returns it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be reloaded.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The reloaded entity.
   */
  protected function reloadEntity(EntityInterface $entity) {
    $controller = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $controller->resetCache([$entity->id()]);
    return $controller->load($entity->id());
  }

  /**
   * Process all the items in a queue.
   *
   * @param string $queue_name
   *   The name of the queue to process.
   */
  protected function processQueue(string $queue_name) {
    $queue_worker = $this->queueManager->createInstance($queue_name);
    $queue = $this->queue->get($queue_name);
    while ($item = $queue->claimItem()) {
      // Don't catch exceptions: we want them to show in the test.
      $queue_worker->processItem($item->data);
      $queue->deleteItem($item);
    }
  }

}
