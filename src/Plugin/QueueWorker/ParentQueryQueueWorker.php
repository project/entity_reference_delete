<?php

namespace Drupal\entity_reference_delete\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\RequeueException;
use Drupal\entity_reference_delete\FieldMap;

/**
 * Queries for parent entities to delete in reaction to child deletion.
 *
 * This runs a limited query for scalability and requeues the current item if
 * more entities remain to be processed.
 *
 * @QueueWorker(
 *   id = "entity_reference_delete_parent_query",
 *   title = "Queries for parent entities to delete in reaction to child deletion.",
 *   cron = {"time" = 60},
 * )
 */
class ParentQueryQueueWorker extends QueueWorkerBase {

  /**
   * The number of entities to query for in each process run.
   *
   * @var int
   */
  protected $queryLimit = 20;

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $parent_entity_type = \Drupal::service('entity_type.manager')->getDefinition($data['parent_entity_type_id']);
    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $parent_entity_storage */
    $parent_entity_storage = \Drupal::service('entity_type.manager')->getStorage($data['parent_entity_type_id']);

    $fields_delete_parents = \Drupal::service('entity_reference_delete.field_map')->getFieldsDeleteParents($data['child_entity_type_id']);
    $field_settings_data = $fields_delete_parents[$data['parent_entity_type_id']][$data['field_name']];

    $query = $parent_entity_storage->getQuery();
    $query->accessCheck(FALSE);

    if (!isset($field_settings_data[FieldMap::ALL_BUNDLES])) {
      $bundles = array_keys($field_settings_data);

      $query->condition($parent_entity_type->getKey('bundle'), $bundles, 'IN');
    }

    // @todo The 'target_id' column name only applies for entity_reference
    // fields.
    $query->condition($data['field_name'] . '.target_id', $data['child_entity_id']);

    // Apply a limit to the query, for scalability. We query for one more than
    // the limit, so we know whether any more entities remain without having to
    // query twice.
    $query->range(0, $this->queryLimit + 1);

    $ids = $query->execute();

    // Requeue if we got one more ID than the query limit, because this means
    // that there is at least one more entity to process in a subsequent run.
    $requeue = (count($ids) >= $this->queryLimit);

    $ids = array_slice($ids, 0, $this->queryLimit);
    $parent_entities_to_delete = $parent_entity_storage->loadMultiple($ids);

    foreach ($parent_entities_to_delete as $entity_to_delete) {
      $entity_to_delete->delete();
    }

    if ($requeue) {
      throw new RequeueException();
    }
  }

}
